package com.greatlearning.designpattern1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBC {
    
	//prevent initialization by constructors
	private JDBC() {}
    
	
	// JDBCConnection hold the singleton instance
	private static class JDBCConnection{
	  private static Connection INSTANCE;
	  static {
		  try {	
		       INSTANCE = DriverManager.getConnection("jdbc:mysql://localhost:3306/user_db","root", "admin");
	      }catch(SQLException ex) {
		       ex.printStackTrace();
	      }
	  }
	}
	
	// global access point to get JDBC Connection
	public static Connection getConnection() {
		return JDBCConnection.INSTANCE;
	}
}
