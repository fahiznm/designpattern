package com.greatlearning.designpattern1;

import java.sql.Connection;

public class Client {

	public static void main(String[] args) {
		
		Connection con1 = JDBC.getConnection();
		Connection con2 = JDBC.getConnection();
		
		
		if(con1.hashCode() == con2.hashCode()) {
			System.out.print("Con1 and Con2 are same connection!");
		}
	}

}
