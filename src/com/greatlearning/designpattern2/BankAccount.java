package com.greatlearning.designpattern2;

public class BankAccount {
  
	//Mandatory attributes
	private String accountNo;
	private String accountType;
	private String branch;
	private double balance;
	
	//Optional attributes
	private String atmTransactions;
	private String emiSchedule;
	
	
	public BankAccount(BankAccountBuilder builder) {
		this.accountNo = builder.accountNo;
		this.accountType = builder.accountType;
		this.branch = builder.branch;
		this.balance = builder.balance;
		this.atmTransactions = builder.atmTransactions;
		this.emiSchedule = builder.emiSchedule;
	}

	public String getAccountNo() {
		return accountNo;
	}
	
	public String getAccountType() {
		return accountType;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public String getAtmTransactions() {
		return atmTransactions;
	}
	
	public String getEmiSchedule() {
		return emiSchedule;
	}
	
	
	
	@Override
	public String toString() {
		return "BankAccount [accountNo=" + accountNo + ", accountType=" + accountType + ", branch=" + branch
				+ ", balance=" + balance + ", atmTransactions=" + atmTransactions + ", emiSchedule=" + emiSchedule
				+ "]";
	}



	public static class BankAccountBuilder{
		//Mandatory attributes
		private String accountNo;
		private String accountType;
		private String branch;
		private double balance;
		
		//Optional attributes
		private String atmTransactions;
		private String emiSchedule;
		
		public BankAccountBuilder(String accountNo, String accountType, String branch, double balance) {
			this.accountNo = accountNo;
			this.accountType = accountType;
			this.branch = branch;
			this.balance = balance;
		}
		
		//set optional field
		public BankAccountBuilder setAtmTransactions(String atmTransactions) {
			this.atmTransactions = atmTransactions;
			return this;
		}
        
		//set optional field
		public BankAccountBuilder setEmiSchedule(String emiSchedule) {
			this.emiSchedule = emiSchedule;
			return this;
		}
        
		// return the finaly construction BankAccount object
		public BankAccount build() {
			BankAccount account = new BankAccount(this);
			return account;
		}
		
	}
	

}
