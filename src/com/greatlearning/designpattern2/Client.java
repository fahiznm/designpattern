package com.greatlearning.designpattern2;

public class Client {
 public static void main(String[] args) {

    // create BankAccount with just mandatory fields
	 BankAccount account1 = new BankAccount.BankAccountBuilder("123", "Savings", "ICIC", 8000).build();
	 
	 System.out.println("Bank Account with just mandatory fields:");
	 System.out.println(account1);
	 
	 // create BankAccount with all fields
	 BankAccount account2 = new BankAccount.BankAccountBuilder("123", "Savings", "ICIC", 8000)
			                .setAtmTransactions("atmTransactions")
			                .setEmiSchedule("Monthly")
			                .build();
	 
	 System.out.println("Bank Account with all fields:");
	 System.out.println(account2);
	 
	 
 }
}
