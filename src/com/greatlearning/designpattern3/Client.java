package com.greatlearning.designpattern3;

public class Client {

  public static void main(String[] args) {
	  // convert 500 GBP to INR
	  INRExchangeService exchange = new INRExchangeService(new GBPConvertor());
	  System.out.println("500 GBP converted to INR is : " + exchange.exchangeCurrency(500) + " Rs");
	  
	  // convert 700 Dollar to INR
	  exchange = new INRExchangeService(new DollarConvertor());
	  System.out.println("700 dollar converted to INR is : " + exchange.exchangeCurrency(700) + " Rs");
  }
}
