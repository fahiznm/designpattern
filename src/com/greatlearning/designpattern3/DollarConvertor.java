package com.greatlearning.designpattern3;

public class DollarConvertor implements ICurrencyConvertor {
	
	// current USD-INR rate
	private static final double DOLLAR_TO_INR_RATE = 74.42;

	public DollarConvertor() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public double convertToINR(double amount) {
		return amount * this.DOLLAR_TO_INR_RATE;
	}
}
