package com.greatlearning.designpattern3;

public interface ICurrencyConvertor {
   public double convertToINR(double amount);
}
