package com.greatlearning.designpattern3;

public class GBPConvertor  implements ICurrencyConvertor{
    
	// Current GBP-INR rate
	private static final double GBP_TO_INR_RATE = 100.88;
	
	public GBPConvertor() {};
 

	@Override
	public double convertToINR(double amount) {
		return amount * this.GBP_TO_INR_RATE;
	}

}
