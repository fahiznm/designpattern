package com.greatlearning.designpattern3;

//INRExchangeService helps to exchange different currencies to INR
public class INRExchangeService {
	
	ICurrencyConvertor currencyConvertor;

	public INRExchangeService(ICurrencyConvertor convertor) {
		this.currencyConvertor = convertor;
	}
	
	public double exchangeCurrency(double amount) {
		return this.currencyConvertor.convertToINR(amount);
	}

}
